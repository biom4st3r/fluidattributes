package com.biom4st3r.fluidattributes.mixin;

import com.biom4st3r.fluidattributes.api.FluidAttributes;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMxn extends Entity {
    public LivingEntityMxn(EntityType<?> type, World world) {
        super(type, world);
        // TODO Auto-generated constructor stub
    }

    /**
     * Implementation pulled from {@link Entity#checkWaterState()}
     */
    @Redirect(at = @At(value = "INVOKE", target = "net/minecraft/entity/LivingEntity.isTouchingWater()Z"), method = "baseTick()V")
    private boolean fabric_extinguishesEntitiy(LivingEntity le)
    {
        return this.isTouchingWater() || this.updateMovementInFluid(FluidAttributes.extinguishes); 
    }

}