package com.biom4st3r.fluidattributes.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import net.minecraft.fluid.Fluid;
import net.minecraft.item.BucketItem;
import net.minecraft.tag.Tag;

@Mixin(BucketItem.class)
public abstract class BucketItemMxn {

    @ModifyVariable(method = "placeFluid",at = @At(value = "FIELD"),)
    public Tag<Fluid> doesVaporize()
    {
        
    }

}