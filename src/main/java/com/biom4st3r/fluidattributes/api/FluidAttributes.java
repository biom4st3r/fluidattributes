package com.biom4st3r.fluidattributes.api;

import net.fabricmc.fabric.api.tag.TagRegistry;
import net.minecraft.fluid.Fluid;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public final class FluidAttributes {

    /**
     * Hurts entities standing in the fluid
     * can be implemented by mods via {@link net.minecraft.block.FluidBlock#onEntityCollision(net.minecraft.block.BlockState, net.minecraft.world.World, net.minecraft.util.math.BlockPos, net.minecraft.entity.Entity)}
     */
    @Deprecated
    public static Tag<Fluid> burns = register("burns");
    /**
     * Ignites entities standing in the fluid<p>
     * can be implemented by mods via {@link net.minecraft.block.FluidBlock#onEntityCollision(net.minecraft.block.BlockState, net.minecraft.world.World, net.minecraft.util.math.BlockPos, net.minecraft.entity.Entity)}
     */
    @Deprecated
    public static Tag<Fluid> ignites = register("ignites");
    /**
     * Hurts and Ignites entities standing in the fluid
     * can be implemented by mods via {@link net.minecraft.block.FluidBlock#onEntityCollision(net.minecraft.block.BlockState, net.minecraft.world.World, net.minecraft.util.math.BlockPos, net.minecraft.entity.Entity)}
     */
    @Deprecated
    public static Tag<Fluid> burnsAndIgnites = register("burns_and_ignites");
    /**
     * Drowneds entities submerged in the fluid.<p>
     * fish can swim in fluid<p>
     * converts zombies to drowned
     */
    public static Tag<Fluid> drown = register("drowns");
    /**
     * Extinguishes burning things in the fluid
     * {@link com.biom4st3r.fluidattributes.mixin.LivingEntityMxn#fabric_extinguishesEntitiy()}
     */
    public static Tag<Fluid> extinguishes = register("extinguishes");
    /**
     * Vaporizes like water in the nether
     * {@link net.minecraft.item.BucketItem#placeFluid(net.minecraft.entity.player.PlayerEntity, net.minecraft.world.World, net.minecraft.util.math.BlockPos, net.minecraft.util.hit.BlockHitResult)}<p>
     * can easily be modified with a ModifyArg
     */
    @Deprecated
    public static Tag<Fluid> vaporizesInNether = register("vaporizes_in_nether");
    /**
     * can be fished in to get loot
     */
    public static Tag<Fluid> contains_loot = register("contains_loot");
    /**
     * can make bubble colums
     */
    public static Tag<Fluid> bubbles = register("bubbles");
    /**
     * keeps coral alive<p>
     * converts concret powder to concret<p>
     * gets removed by sponge
     */
    public static Tag<Fluid> hydrates = register("hydrates");


    private static Tag<Fluid> register(String id)
    {
        return TagRegistry.fluid(new Identifier("fabric",id));
    }
}